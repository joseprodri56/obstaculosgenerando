﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnControl : MonoBehaviour
{ 
    bool selfdestruction= false;
    //Es el tiempo que estara vivo cuando se le active la autodestruccion
    public float timeAlive = 3f;
    // Start is called before the first frame update
    //Es un boolean que uso para indicar al objeto si es seguro spawnear en x zona (No debe de spawnear cerca del enemigo ni encima de otro obstaculo).
    //Vuelve true cuando ya ha pasado por el awake y el collider antes
    bool switching;
    SpriteRenderer show;
    GameObject anotherObs;
    //Transform position;
    private void Awake()
    {
        anotherObs = GameObject.FindGameObjectWithTag("ObstaclePF");
        Physics2D.IgnoreCollision(GetComponent<Collider2D>(), anotherObs.GetComponent<Collider2D>());
        show = this.gameObject.GetComponent<SpriteRenderer>();
        switching = false;
        show.enabled = !show.enabled;
    }
    private void Start()
    {

       // position = this.GetComponent<Transform>();
    }


    // Update is called once per frame
    void Update()
    {
        switching = true;
        print(this.gameObject.name);
        if (!show.isVisible)
        {
            show.enabled = !show.enabled;
        }
        if (selfdestruction)
        {
            timeAlive -= Time.deltaTime;

        }
        if (timeAlive <= 0)
        {
            Destroy(gameObject);
            generateObsta.nObj = generateObsta.nObj - 1;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Aqui se tendra que poner el nombre de la IA
        if(collision.gameObject.name == "Square")
        {
            selfdestruction = true;
        }
        if(collision.gameObject.tag == "ObstaclePF")
        {
            GameObject col = collision.gameObject;
            //Physics2D.IgnoreCollision(GetComponent<Collider2D>(), col.GetComponent<Collider2D>());
            //GameObject ignored = collision.gameObject;
            //Physics2D.IgnoreCollision(collision, GetComponent<Collider2D>());
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "SpawnKill")
        {
            if (!switching)
            {

                Destroy(gameObject);
                this.gameObject.SetActive(true);
                generateObsta.nObj = generateObsta.nObj - 1;
            }

        }
    }


}
